﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;

namespace Briljant.Filters
{
    public class AuthenticationAttribute : Attribute, IAuthenticationFilter
    {
        public string realm { get; set; }
        public bool AllowMultiple => false;

        public async Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            var request = context.Request;
            var authorization = request.Headers.Authorization;

            if (authorization == null || authorization.Scheme != "Bearer")
                return;

            if (string.IsNullOrEmpty(authorization.Parameter))
            {
                context.ErrorResult = new AuthenticationFailureResult("Missing Jwt Token", request);
                return;
            }

            var token = authorization.Parameter;
            var principal = await AuthenticateJwtToken(token);
            if (principal == null)
                context.ErrorResult = new AuthenticationFailureResult("Invalid token", request);
            else
                context.Principal = principal;
        }
        private static bool ValidateToken(string token, out string username)
        {
            username = null;

            var securityToken = JwtManager.GetPrincipal(token);

            if (securityToken == null)
                return false;

            DateTimeOffset dateTimeOffset = DateTimeOffset.FromUnixTimeSeconds((long)(securityToken.Payload.Exp));
            System.Diagnostics.Debug.WriteLine("exp: " + dateTimeOffset.DateTime);
            System.Diagnostics.Debug.WriteLine("exp local: " + dateTimeOffset.DateTime.ToLocalTime());

            bool isExpired = dateTimeOffset.DateTime.ToLocalTime() < DateTime.Now;

            if (isExpired) return false;

            var uniqueIdClaim = securityToken.Payload.Claims.Single(claim => claim.Type.Equals("uniqueId"));
            username = uniqueIdClaim.Value;

            if (string.IsNullOrEmpty(username))
                return false;

            return true;
        }

        protected Task<IPrincipal> AuthenticateJwtToken(string token)
        {
            if (ValidateToken(token, out var username))
            {
                // based on username to get more information from database in order to build local identity
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, username)
                    // Add more claims if needed: Roles, ...
                };

                var identity = new ClaimsIdentity(claims, "Jwt");
                IPrincipal user = new ClaimsPrincipal(identity);

                return Task.FromResult(user);
            }

            return Task.FromResult<IPrincipal>(null);
        }

        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            Challenge(context);
            return Task.FromResult(0);
        }

        private void Challenge(HttpAuthenticationChallengeContext context)
        {
            context.ChallengeWith("Bearer", "realm=\"kamino\"");
            /*string parameter = null;
            if (!string.IsNullOrEmpty(Realm))
                parameter = "realm=\"" + Realm + "\"";
            context.ChallengeWith("Bearer", parameter);*/
        }
    }
}