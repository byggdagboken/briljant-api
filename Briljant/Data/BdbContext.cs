﻿using Briljant.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Briljant.Data
{
    public class BdbContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public BdbContext() : base("name=BdbContext")
        {
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<BdbContext, EF6Console.Migrations.Configuration>());
        }

        public System.Data.Entity.DbSet<User> Users { get; set; }
        public System.Data.Entity.DbSet<Transaction> Transactions { get; set; }
        public System.Data.Entity.DbSet<TransactionVerification> TransactionVerifications { get; set; }
        public System.Data.Entity.DbSet<StatusLog> Logs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}