﻿using System;
using System.Web.Http;

namespace Briljant.Controllers
{
    [RoutePrefix("briljant/health")]
    public class HealthController : ApiController
    {
        [AllowAnonymous]
        [HttpGet]
        [Route("")]
        public String Index()
        {
            return "UP";
        }
    }
}