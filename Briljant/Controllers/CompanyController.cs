﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Web.Http;
using System.Linq;
using Briljant.Data;
using Briljant.Models;
using Briljant.Webb;
using Briljant.Filters;

namespace Briljant.Controllers
{
    [RoutePrefix("briljant/companies")]
    public class CompanyController : ApiController
    {
        private BdbContext db = new BdbContext();

        [Authentication]
        [HttpGet]
        [Route("")]
        public IEnumerable<AllowedCompany> Index(string companyId)
        {
            String status = "";
            User user = db.Users.Where(u => u.companyId.Equals(companyId)).FirstOrDefault();
            AllowedCompany[] companies = new AllowedCompany[] { };

            if (user != null)
            {
                try
                {
                    string username = user.username;
                    APIServiceClient apiClient = new APIServiceClient("BriljantAPI", user.endpoint);
                    apiClient.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
                    apiClient.ClientCredentials.UserName.UserName = @username;
                    apiClient.ClientCredentials.UserName.Password = user.password;
                    companies = apiClient.GetAllowedCompanies(ref status);
                    apiClient.Close();
                    apiClient = null;
                } catch(Exception e)
                {
                    throw e;
                }
            }

            return companies;
        }
    }
}