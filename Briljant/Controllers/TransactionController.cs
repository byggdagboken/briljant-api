﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Briljant.Models;
using Briljant.Webb;
using Briljant.Data;
using Briljant.Filters;

namespace Briljant.Controllers
{
    [RoutePrefix("briljant/transactions")]
    public class TransactionController : ApiController
    {
        private BdbContext db = new BdbContext();

        [Authentication]
        [HttpGet]
        [Route("")]
        public List<TransactionResult> GetTransactions(String companyId)
        {
            List<Transaction>  list = new List<Transaction>();

            IQueryable<TransactionResult> query = (
                from t in db.Transactions
                join l in db.Logs on t.uniqueId equals l.transactionId into gj
                from logs in gj.DefaultIfEmpty()
                where t.companyId.Equals(companyId)
                select new TransactionResult
                {
                    uniqueId = t.uniqueId,
                    companyId = t.companyId,
                    verificationType = t.verificationType,
                    start = t.start,
                    end = t.end,
                    createdTimestamp = t.createdTimestamp,
                    status = logs.statusMessage
                }
            );

            return query.ToList();
        }

        [Authentication]
        [HttpPost]
        [Route("")]
        public IEnumerable<string> Index(Transaction transaction)
        {
            User user = db.Users.Where(u => u.companyId.Equals(transaction.companyId)).FirstOrDefault();

            if (user == null)
            {
                // TODO: 404
            }

            string username = user.username;

            transaction.uniqueId = Guid.NewGuid().ToString();

            if (transaction.createdTimestamp == null)
            {
                transaction.createdTimestamp = DateTime.Now;
            }

            if (transaction.creatingUserId == null)
            {
                transaction.creatingUserId = User.Identity.Name;
            }

            db.Transactions.Add(transaction);
            db.SaveChanges();

            APIServiceClient apiClient = new APIServiceClient("BriljantAPI", user.endpoint);
            apiClient.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            apiClient.ClientCredentials.UserName.UserName = @username;
            apiClient.ClientCredentials.UserName.Password = user.password;

            String status = "";
            AllowedCompany[] companies = apiClient.GetAllowedCompanies(ref status);
            LogStatus(transaction, "GetAllowedCompanies", status);

            AllowedCompany company = companies.FirstOrDefault(c => c.CompanyID == transaction.briljantCompanyId);

            if (company == null)
            {
                status = "";
                LogStatus(transaction, "AllowedCompany is null", status);
            }
            else
            {
                AddTimeTransaction(transaction, company, user);
            }

            apiClient.Close();
            apiClient = null;

            return new string[] { "value", "value2" };
        }

        public void AddTimeTransaction(Transaction transaction, AllowedCompany company, User apiUser)
        {
            List<PrelTimeTransactions> timeTransactionList = GetPresenceReports(transaction);

            /*switch (transaction.reportType)
            {
                case "All":
                    timeTransactionList.Concat(GetPresenceReports(transaction)).Concat(GetAbsenceReports(transaction));
                    break;
                case "Presence":
                    timeTransactionList = GetPresenceReports(transaction);
                    break;
                case "Absence":
                    timeTransactionList = GetAbsenceReports(transaction);
                    break;
            }*/

            PrelTimeTransactions[] timeTransactions = timeTransactionList.ToArray();

            APIServiceClient client = new APIServiceClient("BriljantAPI", apiUser.endpoint);
            client.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;
            string username = apiUser.username;
            client.ClientCredentials.UserName.UserName = @username;
            client.ClientCredentials.UserName.Password = apiUser.password;

            String status = "";
            int[] addedRows = client.AddTimeTransactionRows(company, timeTransactions, ref status);

            LogStatus(transaction, "AddTimeTransactionRows", status);

            if (addedRows.Length > 0)
            {
                foreach (int verificationNumber in addedRows)
                {
                    SaveVerification(transaction, verificationNumber);
                }
            }

            client.Close();
            client = null;
        }

        private void LogStatus(Transaction transaction, String action, String status)
        {
            var log = new StatusLog();
            log.companyId = transaction.companyId;
            log.transactionId = transaction.uniqueId;
            log.action = action;
            log.statusMessage = status;
            log.createdTimestamp = DateTime.Now;
            log.creatingUserId = User.Identity.Name;
            db.Logs.Add(log);
            db.SaveChanges();
        }

        private void SaveVerification(Transaction transaction, int verificationNumber)
        {
            TransactionVerification transactionVerification = new TransactionVerification();
            transactionVerification.uniqueId = Guid.NewGuid().ToString();
            transactionVerification.transactionId = transaction.uniqueId;
            transactionVerification.verificationNumber = verificationNumber;
            transactionVerification.createdTimestamp = DateTime.Now;
            transactionVerification.creatingUserId = User.Identity.Name;
            db.TransactionVerifications.Add(transactionVerification);
            db.SaveChanges();
        }

        public List<PrelTimeTransactions> GetPresenceReports(Transaction transaction)
        {
            List<PrelTimeTransactions> timeTransactions = new List<PrelTimeTransactions>();

            String url = "https://" + transaction.subdomain + ".byggdagboken.nu/php/ajax/index.php?page=salaryintegrations&action=timereportsbycompanyperiod";
            StringBuilder urlBuilder = new StringBuilder(url);
            urlBuilder.Append("&companyId=" + transaction.companyId);
            urlBuilder.Append("&start=" + transaction.start.Value.ToString("yyyy-MM-dd"));
            urlBuilder.Append("&end=" + transaction.end.Value.ToString("yyyy-MM-dd"));
            urlBuilder.Append("&onlySigned=" + (transaction.isSignedRequired ? "1" : "0"));

            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("X-Api-Key", "8b617a71-43fc-48c5-9df0-565dffe0c5bf");

            var response = httpClient.GetAsync(urlBuilder.ToString());
            var result = response.Result;

            if (result.IsSuccessStatusCode)
            {
                String str = result.Content.ReadAsStringAsync().Result;
                BdbResponse bdbResponse = JsonConvert.DeserializeObject<BdbResponse>(str);

                for (int i = 0; i < bdbResponse.Data.Items.Length; i++)
                {
                    TimeReport timeReport = bdbResponse.Data.Items[i];

                    decimal amount = 0;

                    if (timeReport.IsNumeric)
                    {
                        decimal.TryParse(timeReport.Value, out amount);

                        if (amount.Equals(0))
                        {
                            decimal.TryParse(timeReport.Value.Replace(".", ","), out amount);
                        }
                    }
                    else
                    {
                        decimal.TryParse(timeReport.Value, out amount);

                        if (amount.Equals(0))
                        {
                            decimal.TryParse(timeReport.Value.Replace(".", ","), out amount);
                        }
                    }

                    String date = timeReport.Date.Date.ToString("yyyyMMdd");
                    int trdat;
                    short eventCodeExternalId;
                    int cost;

                    int.TryParse(date, out trdat);
                    short.TryParse(timeReport.EventCodeExternalId, out eventCodeExternalId);
                    int.TryParse(timeReport.Cost, out cost);

                    if (eventCodeExternalId == 0) continue;

                    var prelTimeTransaction = new PrelTimeTransactions();
                    prelTimeTransaction.proj = timeReport.Project.Number;
                    prelTimeTransaction.ant = amount;
                    prelTimeTransaction.art = (short)eventCodeExternalId;
                    prelTimeTransaction.trdat = trdat;
                    prelTimeTransaction.pris = cost;
                    prelTimeTransaction.lnr = 0;
                    prelTimeTransaction.bel = 0;
                    // Anställd?
                    prelTimeTransaction.typ = 2;
                    prelTimeTransaction.nr = timeReport.Employee.EmploymentNumber;
                    prelTimeTransaction.bunt = 0;
                    prelTimeTransaction.flag = 0;
                    prelTimeTransaction.intpris = 0;
                    prelTimeTransaction.kostn = 0;
                    prelTimeTransaction.otid = 0;
                    prelTimeTransaction.ptid = 0;
                    prelTimeTransaction.ver = 0;
                    prelTimeTransaction.vtyp = (byte)transaction.verificationType;

                    timeTransactions.Add(prelTimeTransaction);
                }
            }

            return timeTransactions;
        }

        /*public List<PrelTimeTransactions> GetAbsenceReports(Transaction transaction)
        {
            List<PrelTimeTransactions> timeTransactions = new List<PrelTimeTransactions>();

            StringBuilder urlBuilder = new StringBuilder(transaction.subdomain + "/php/ajax/index.php?page=salaryintegrations&action=absencebycompanyperiod");
            urlBuilder.Append("&companyId=" + transaction.companyId);
            urlBuilder.Append("&start=" + transaction.start);
            urlBuilder.Append("&end=" + transaction.end);
            urlBuilder.Append("&onlySigned=" + (transaction.isSignedRequired ? "1" : "0"));

            String url = urlBuilder.ToString();

            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Add("X-Api-Key", "8b617a71-43fc-48c5-9df0-565dffe0c5bf");
            var response = httpClient.GetAsync(url);
            var result = response.Result;

            if (result.IsSuccessStatusCode)
            {
                String str = result.Content.ReadAsStringAsync().Result;
                BdbResponse bdbResponse = JsonConvert.DeserializeObject<BdbResponse>(str);

                for (int i = 0; i < bdbResponse.Data.Items.Length; i++)
                {
                    TimeReport timeReport = bdbResponse.Data.Items[i];

                    String date = timeReport.Date.Date.ToString("yyyyMMdd");
                    int trdat;
                    int.TryParse(date, out trdat);

                    var prelTimeTransaction = new PrelTimeTransactions();
                    prelTimeTransaction.proj = timeReport.Project.Number;
                    prelTimeTransaction.ant = timeReport.Value;

                    short eventCodeExternalId;
                    short.TryParse(timeReport.EventCodeExternalId, out eventCodeExternalId);
                    int cost;
                    int.TryParse(timeReport.Cost, out cost);

                    prelTimeTransaction.art = (short) eventCodeExternalId;
                    prelTimeTransaction.trdat = trdat;
                    prelTimeTransaction.pris = cost;
                    prelTimeTransaction.lnr = 0;
                    prelTimeTransaction.bel = 0;
                    // Anställd?
                    prelTimeTransaction.typ = 2;
                    prelTimeTransaction.nr = timeReport.Employee.EmploymentNumber;
                    prelTimeTransaction.bunt = 0;
                    prelTimeTransaction.flag = 0;
                    prelTimeTransaction.intpris = 0;
                    prelTimeTransaction.kostn = 0;
                    prelTimeTransaction.otid = 0;
                    prelTimeTransaction.ptid = 0;
                    prelTimeTransaction.ver = 0;
                    prelTimeTransaction.vtyp = (byte) transaction.verificationType;

                    timeTransactions.Add(prelTimeTransaction);
                }
            }

            return timeTransactions;
        }*/
    }
}