using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using Briljant.Data;
using Briljant.Filters;
using Briljant.Models;

namespace Briljant.Controllers
{
    [RoutePrefix("briljant/users")]
    public class UsersController : ApiController
    {
        private BdbContext db = new BdbContext();

        [Authentication]
        [HttpGet]
        [Route("")]
        //TODO: throws exception if no user exists
        public IHttpActionResult GetUsers(String companyId)
        {
            return db.Users.Where(u => u.companyId.Equals(companyId));
            var users = db.Users.Where(u => u.companyId.Equals(companyId));
            return Ok(users);
        }

        [Authentication]
        [HttpGet]
        [Route("{id}", Name = "Get")]
        [ResponseType(typeof(User))]
        public IHttpActionResult GetUser(int id)
        {
            User user = db.Users.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        [Authentication]
        [HttpPut]
        [Route("{id}")]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutUser(String id, User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != user.uniqueId)
            {
                return BadRequest();
            }

            db.Entry(user).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [Authentication]
        [HttpPost]
        [Route("")]
        [ResponseType(typeof(User))]
        public IHttpActionResult PostUser(User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            user.uniqueId = Guid.NewGuid().ToString();

            if (user.createdTimestamp == null)
            {
                user.createdTimestamp = DateTime.Now;
            }

            if (user.creatingUserId == null)
            {
                user.creatingUserId = User.Identity.Name;
            }

            db.Users.Add(user);
            db.SaveChanges();

            return CreatedAtRoute("Get", new { id = user.id }, user);
        }

        [Authentication]
        [HttpDelete]
        [Route("{id}")]
        [ResponseType(typeof(User))]
        public IHttpActionResult DeleteUser(String id)
        {
            //User user = db.Users.Find(id);
            User user = db.Users.Where(u => u.uniqueId.Equals(id)).Single();
            if (user == null)
            {
                return NotFound();
            }

            db.Users.Remove(user);
            db.SaveChanges();

            return Ok(user);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UserExists(String uniqueId)
        {
            return db.Users.Count(e => e.uniqueId == uniqueId) > 0;
        }
    }
}