﻿using System;
using System.Web.Http;

namespace Briljant.Controllers
{
    [RoutePrefix("briljant/version")]
    public class VersionController : ApiController
    {
        [AllowAnonymous]
        [HttpGet]
        [Route("")]
        public String Index()
        {
            return "V1.1";
        }
    }
}