﻿using System;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;

namespace Briljant
{
    public static class JwtManager
    {
        private const string Secret = "Yq3t6v9y$B&E)H@McQfTjWnZr4u7x!z%";

        public static JwtSecurityToken GetPrincipal(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

                if (jwtToken == null)
                    return null;

                var securityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Secret));

                var validationParameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = securityKey
                };

                var principal = tokenHandler.ValidateToken(token, validationParameters, out var validToken);
                System.Diagnostics.Debug.WriteLine(validToken);

                return validToken as JwtSecurityToken;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}