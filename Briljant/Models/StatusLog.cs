﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Briljant.Models
{
    [Table("status_log")]
    public class StatusLog
    {
        [Column("id")]
        public int id { get; set; }

        [Column("company_id")]
        public string companyId { get; set; }

        [Column("transaction_id")]
        public string transactionId { get; set; }

        [Column("action")]
        public string action { get; set; }

        [Column("status_message")]
        public string statusMessage { get; set; }

        [Column("created_timestamp")]
        public DateTime? createdTimestamp { get; set; }

        [Column("creating_user_id")]
        public string creatingUserId { get; set; }
    }
}