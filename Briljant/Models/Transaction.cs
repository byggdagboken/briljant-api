﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Briljant.Models
{
    [Table("transaction")]
    public class Transaction
    {
        [Column("id")]
        public int id { get; set; }

        [Column("unique_id")]
        public String uniqueId { get; set; }

        [Column("company_id")]
        public string companyId { get; set; }

        [Column("briljant_company_id")]
        public int briljantCompanyId { get; set; }

        [Column("report_type")]
        public string reportType { get; set; }

        [Column("subdomain")]
        public string subdomain { get; set; }

        [Column("verification_type")]
        public int verificationType { get; set; }

        [Column("is_signed_required")]
        public bool isSignedRequired { get; set; }

        [Column("start")]
        public DateTime? start { get; set; }

        [Column("end")]
        public DateTime? end { get; set; }

        [Column("created_timestamp")]
        public DateTime? createdTimestamp { get; set; }

        [Column("creating_user_id")]
        public string creatingUserId { get; set; }

        public enum ReportType {
            All,
            Presence,
            Absence
        }
    }
}