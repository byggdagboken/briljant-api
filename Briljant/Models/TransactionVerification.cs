﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Briljant.Models
{
    [Table("transaction_verification")]
    public class TransactionVerification
    {
        [Column("id")]
        public int id { get; set; }

        [Column("unique_id")]
        public String uniqueId { get; set; }

        [Column("transaction_id")]
        public String transactionId { get; set; }

        [Column("verification_number")]
        public int verificationNumber { get; set; }

        [Column("created_timestamp")]
        public DateTime? createdTimestamp { get; set; }

        [Column("creating_user_id")]
        public string creatingUserId { get; set; }
    }
}