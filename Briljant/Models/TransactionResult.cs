﻿using System;

namespace Briljant.Models
{
    public class TransactionResult
    {
        public String uniqueId { get; set; }

        public string companyId { get; set; }

        public int verificationType { get; set; }

        public bool isSignedRequired { get; set; }

        public DateTime? start { get; set; }

        public DateTime? end { get; set; }

        public DateTime? createdTimestamp { get; set; }
        public string creatingUserId { get; set; }
        public String status { get; set; }
    }
}