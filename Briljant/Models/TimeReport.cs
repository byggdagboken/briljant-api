﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Briljant.Models
{
    public class TimeReport
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("employee")]
        public Employee Employee { get; set; }

        [JsonProperty("date")]
        public DateTimeOffset Date { get; set; }

        [JsonProperty("presence_code_id")]
        public Guid PresenceCodeId { get; set; }

        [JsonProperty("event_code_external_id")]
        public String EventCodeExternalId { get; set; }

        [JsonProperty("value")]
        public String Value { get; set; }

        [JsonProperty("cost")]
        public String Cost { get; set; }

        [JsonProperty("is_numeric")]
        public bool IsNumeric { get; set; }

        public Project Project { get; set; }
    }

    public partial class Employee
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("employment_number")]
        //[JsonConverter(typeof(JsonConverter))]
        public int EmploymentNumber { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public partial class Project
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("number")]
        //[JsonConverter(typeof(JsonConverter))]
        public long Number { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }
}