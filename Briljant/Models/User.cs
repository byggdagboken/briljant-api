﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Briljant.Models
{
    [Table("user")]
    public class User
    {
        [Column("id")]
        public int id { get; set; }

        [Column("unique_id")]
        public string uniqueId { get; set; }

        [Column("company_id")]
        public string companyId { get; set; }

        [Column("username")]
        public string username { get; set; }

        [Column("password")]
        public string password { get; set; }

        [Column("endpoint")]
        public string endpoint { get; set; }

        [Column("created_timestamp")]
        public DateTime? createdTimestamp { get; set; }

        [Column("creating_user_id")]
        public string creatingUserId { get; set; }
    }
}