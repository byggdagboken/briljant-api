﻿using System;

namespace Briljant.Models
{
    public class ReportModel
    {
        public String companyId { get; set; }
        public int briljantCompanyId { get; set; }
        public int verificationType { get; set; }
        public String reportType { get; set; }
        public String subdomain { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
    }
}