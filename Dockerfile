#Depending on the operating system of the host machines(s) that will build or run the containers, the image specified in the FROM statement may need to be changed.
#For more information, please see https://aka.ms/containercompat 

FROM microsoft/dotnet-framework:4.7.2-sdk-windowsservercore-ltsc2016 AS build
WORKDIR /app

# copy csproj and restore as distinct layers
COPY *.sln .
COPY Briljant/*.csproj ./Briljant/
COPY Briljant/*.config ./Briljant/
RUN nuget restore

# copy everything else and build app
COPY Briljant/. ./Briljant/
WORKDIR /app/Briljant
RUN msbuild /p:Configuration=Debug

FROM microsoft/aspnet:4.7.2-windowsservercore-ltsc2016 AS runtime
WORKDIR /inetpub/wwwroot
COPY --from=build /app/Briljant/. ./
